from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from datetime import datetime, timedelta
import telebot
from telebot import types
import pymysql
import pymysql.cursors
import random

API_TOKEN='6254619414:AAHes6zKShu3UZHdNLsT9r5GxxRoEkLShzg'
bot = telebot.TeleBot(API_TOKEN)
alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
numbersss='0123456789'
d_cost={}
d_discription={}
#d_cost[f'{message.from_user.username}'] = 0
#d_discription[f'{message.from_user.username}'] = ''
connection = pymysql.connect(
    host='188.225.46.42',
    user='gen_user',
    password='dimfzuwqa2',
    db='default_db')


@bot.message_handler()
def answer(message):
    global d_cost, d_discription
    if message.text == '/start':
        d_cost[f'{message.from_user.username}'] = 0
        d_discription[f'{message.from_user.username}'] = ''
        d_discription[f'{message.from_user.username}_status'] = 0
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        button1 = types.KeyboardButton('Как это работает?')
        markup.add(button1)
        photo = open(random.choice(['./mem1.jpg', './mem2.jpg', './mem3.jpg', './mem4.jpg', './mem5.jpg']), 'rb')
        bot.send_photo(message.chat.id, photo=photo, reply_markup=markup)
        bot.send_message(message.chat.id, 'Привет! Планируешь свой бюджет? Наш бот-поможет тебе в этом!',reply_markup=markup)
    elif message.text == 'Как это работает?':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        button2 = types.KeyboardButton('Поехали!')
        markup.add(button2)
        bot.send_message(message.chat.id, 'Вы ежедневно вводите свои покупки и их стоимость, а я все остальное делаю за Вас: веду учет, анализирую расходы, предоставляю отчеты. Приступим?',reply_markup=markup)
    elif message.text == 'Поехали!':
        bot.send_message(message.chat.id, 'Введите название расхода: ')
    elif message.text == 'Удалить мою историю записей':
        cursor = connection.cursor()
        cursor.execute(f"DELETE FROM Buy WHERE username = '{message.from_user.username}'")
        connection.commit()
        bot.send_message(message.chat.id, 'История расходов удалена. Можете вводить название расхода: ')
        d_cost[f'{message.from_user.username}'] = 0
        d_discription[f'{message.from_user.username}'] = ''
        d_discription[f'{message.from_user.username}_status']=0
    else:
        if (message.text[0].lower() in alphabet) and d_cost[f'{message.from_user.username}']==0:
            bot.send_message(message.chat.id, 'Вы ввели название расхода. Теперь введите стоимость: ')
            d_discription[f'{message.from_user.username}'] = message.text
            d_discription[f'{message.from_user.username}_status'] = 1
        elif (message.text[0] in numbersss) and d_discription[f'{message.from_user.username}_status']==1:
            d_cost[f'{message.from_user.username}'] = message.text
            current_date = datetime.now().date()
            date_str = current_date.strftime('%Y-%m-%d')
            #отправялем данные в БД
            cursor = connection.cursor()
            sql = "INSERT INTO Buy (username, date, discription, cost) VALUES (%s, %s, %s, %s)"
            val = (message.from_user.username, date_str, d_discription[f'{message.from_user.username}'], d_cost[f'{message.from_user.username}'])
            cursor.execute(sql, val)
            connection.commit()
            # запрашиваем данные из БД (последние 10 строк)
            cursor = connection.cursor()
            cursor.execute(f"SELECT COUNT(*) FROM Buy WHERE username = '{message.from_user.username}'")
            size_DB = cursor.fetchall()[0][0]
            if size_DB<20:
                size_DB=size_DB
            else:
                size_DB=20
            connection.commit()
            cursor = connection.cursor()
            cursor.execute(f"SELECT * FROM Buy WHERE username = '{message.from_user.username}' ORDER BY N DESC LIMIT {size_DB}")
            result = cursor.fetchall()
            connection.commit()
            image = Image.open("./gross.jpg")
            draw = ImageDraw.Draw(image)
            font = ImageFont.truetype("DejaVuSerif.ttf", 22)
            # отрисовываем изображение
            #верхний блок
            for i in range(0,size_DB):
                date_str=result[i][2]
                prod = result[i][3]
                price = result[i][4]
                draw.text((80, 120+25*i), f"{date_str}", (255, 255, 255), font=font, bold=True)
                draw.text((245, 120+25*i), f"{prod}", (255, 255, 255), font=font, bold=True)
                draw.text((470, 120+25*i), f"{price}", (255, 255, 255), font=font, bold=True)
            #нижний блок
            font2 = ImageFont.truetype("DejaVuSerif.ttf", 22)
            #Cчитаем и отрисовываем сумму за все время
            cursor = connection.cursor()
            cursor.execute(f"SELECT SUM(cost) FROM Buy WHERE username = '{message.from_user.username}'")
            summ_all_time = cursor.fetchall()[0][0]
            connection.commit()
            draw.text((75, 120 + 25 * 25), f"Сумма расходов за все время: {summ_all_time}", (255, 255, 255), font=font2, bold=True)
            #Cчитаем и отрисовываем сумму за текущий месяц
            #отправка SQL запроса
            cursor = connection.cursor()
            cursor.execute(f"SELECT SUM(cost) FROM Buy WHERE username = '{message.from_user.username}' AND MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE())")
            summ_30_days = cursor.fetchone()[0]
            connection.commit()
            draw.text((75, 800), f"Сумма расходов в текущем месяце: {summ_30_days}", (255, 255, 255), font=font2,bold=True)
            #отправка отрисованного изображения
            image.save(f"{message.chat.id}_gross2.jpg")
            bot.send_photo(message.chat.id, photo=open(f"{message.chat.id}_gross2.jpg", 'rb'))
            markup3 = types.ReplyKeyboardMarkup(resize_keyboard=True)
            button3 = types.KeyboardButton('Удалить мою историю записей')
            markup3.add(button3)
            bot.send_message(message.chat.id, 'Можете вводить следующее название расхода: ', reply_markup=markup3)
            #bot.send_message(message.chat.id, 'Вы ввели стоимость. Ваши последние покупки : ')
            d_cost[f'{message.from_user.username}'] = 0
            d_discription[f'{message.from_user.username}'] = ''
            d_discription[f'{message.from_user.username}_status'] = 0
        else:
            if d_discription[f'{message.from_user.username}_status']==0:
                bot.send_message(message.chat.id, 'Сначала введи название расхода на русском языке')
                photo = open(random.choice(['./mem1.jpg', './mem2.jpg', './mem3.jpg', './mem4.jpg', './mem5.jpg']), 'rb')
                bot.send_photo(message.chat.id, photo=photo)
bot.polling(none_stop=True)

